import React from 'react';
import { Field } from 'redux-form';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';

import TextField from 'material-ui/TextField';

export const renderTextField = ({ input, label, meta: { touched, error }, ...custom }) => (
  <TextField hintText={label}
    floatingLabelText={label}
    errorText={touched && error}
    {...input}
    {...custom}
  />
)

const style = {
  width: '100%',
  padding: 20,
  textAlign: 'center',
  display: 'inline-block',
};

const subHeaderStyle = {
  fontWeight: 'bold',
  textTransform: 'uppercase',
  fontSize: '30px'
}

let SearchForm = props => {
  const { handleSubmit, pristine, submitting } = props;
  console.log(props);
  return (
    <div>
      <AppBar
        title="Search a number"
        showMenuIconButton={false}
      />
      <Paper style={style} zDepth={1}>
        <form onSubmit={handleSubmit}>
          <div>
            <Field 
              name="key" 
              component={renderTextField} 
              label="Enter number to search" 
              style={{width:'100%'}}/>
          </div>
          <div>
            <RaisedButton 
              type="submit"
              label="Search" 
              secondary={true} 
              disabled={pristine || submitting}
              style={{width:'100%'}}/>
          </div>
        </form>
        <Divider />
        <Subheader 
          style={{...subHeaderStyle, color: props.result.color}}>
          
          {props.result.value}
        </Subheader>
      </Paper>
    </div>
  )
}

export default SearchForm;
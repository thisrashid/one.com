// import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import SearchForm from './ui/search.ui';

const form = 'search';
const validate = values => {
  const errors = {}
  if(!values.key) {
    errors.key = 'Please enter a number to search';
  }
  return errors;
}

export default reduxForm({
  form,  // a unique identifier for this form
  validate,
  // asyncValidate
})(SearchForm)

import React, { Component } from 'react';
import Search from './search.component';
import { getChildren } from '../services/lookup.service';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      result: '',
      started: false,
      index: 0,
      found: false
    }
    this.submit = this.submit.bind(this);
    this.search = this.search.bind(this);

    this.count = 0;
  }

  setResult(found = false, path = 'Not Found') {
    this.setState({
      result: {
        value: found ? `Found at: ${path}` : path,
        color: found ? '#4CAF50' : '#D50000',
      }
    });
  }
  normalizePath(path) {
    return path.replace('//', '/');
  }

  extract(elem) {
    const path = Object.keys(elem)[0],
      nodes = elem[path];
    return { path, nodes };
  }

  search(key, map) {
    let p = [];
    for(let x in map) {
      const { path, nodes} = this.extract(map[x]);

      if(nodes.indexOf(key) >-1) {
        return this.setResult(true, path);
      } else {
        const promises = nodes.map(node => {
          return {
            path: path + '/' + node,
            promise: getChildren(path + '/' + node)
          };
        });
        p = p.concat(promises);
      }
    }

    const promises = p.map(x => x.promise);
    
    Promise.all(promises).then(results => {
      const nextMap = results.map((result, idx) => {
        return { [this.normalizePath(p[idx].path)]: result };
      });

      return nextMap.length ? this.search(key, nextMap) : this.setResult(false);
    });
  }

  submit(values) {
    getChildren('').then(c => {
      this.search(values.key, [{'/' : c}]);
    });
  }

  render() {
    return (
        <Search 
          onSubmit={this.submit} 
          result={this.state.result}
        />
    );
  }
}


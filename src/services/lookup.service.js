const TreeLookup = window.TreeLookup;
const lookup = new TreeLookup();

export const getChildren = (path = '/') => {
  return lookup.getChildrenAsPromise(path);
}
import { 
  createStore, 
  combineReducers } from 'redux';
import { reducer as form } from 'redux-form';

let store = createStore(
  combineReducers({ form })
);

export default store;
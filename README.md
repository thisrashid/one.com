# Search a Number #

This application will search a number from the tree using BFS.

### Installation ###
```
 $ git clone https://thisrashid@bitbucket.org/thisrashid/one.com.git
 $ cd one.com
 $ npm install
```
After installation of all dependencies, execute ``` $ npm start``` for local development.

To create production build, execute ```$ npm run build```. After its completion deploy build folder.